# ExLibrisApi

A basic wrapper for Ex Libris API:s (currently [Alma's API:s](https://developers.exlibrisgroup.com/alma/apis) only). Features include:

* Handling of HTTP requests
* Handling of the concurrent API requests threshold
* A very limited number of methods wrapping calls to Alma's API:s
* A couple of convenience methods for threaded retrieval of holdings data

## Usage

```ruby
api = ExLibrisAPI::API.new(
    api_key: <some key>,

    # These are the default values:
    base_uri: 'https://api-eu.hosted.exlibrisgroup.com',
    format: 'application/json;charset=UTF-8',    # Or 'application/xml'
    parse: true,                                 # Whether to parse the response into a Ruby hash
    retries: 5                                   # Number of attempts upon reaching the concurrent API requests threshold
)

user = api.get_user_details(
    user_id: <some id>,
    params: <URL params hash, if any>,
    options: <hash to override default settings like :format etc.>
)

user['first_name'] = 'foo'

api.update_user_details(
    user_id: user['primary_id'],
    body: user, # Also works with JSON or XML data in string form
    params: <URL params hash, if any>,
    options: <hash to override default settings like :format etc.>
)
```

Etc.

### Convenience methods

```ruby
holdings = api.x_get_holdings(
    mms_ids: <MMS ID:s separated by comma>,
    sort_by: 'a,b,c',
    reverse: 'b',
    group_by: 'a',
    include_items: true # default
)
```
