# coding: utf-8
require 'json'
require 'nori'
require 'gyoku'

module ExLibrisAPI
  refine Array do
    def nested_group_by(*keys)
      return self if keys.length == 0
      groups = group_by(&keys.shift)
      Hash[groups.map { | k, v | [k, v.nested_group_by(*keys)] }]
    end
  end

  class API
    using ExLibrisAPI
    attr_accessor :options

    @@xml_parser = Nori.new

    def initialize(
          api_key:,
          base_uri: 'https://api-eu.hosted.exlibrisgroup.com',
          format: 'application/json;charset=UTF-8',
          parse: true,
          retries: 5,
          debug: false
        )
      @api_key = api_key
      @base_uri = base_uri
      @format = format
      @parse = parse
      @retries = retries
      @debug = debug
    end

    def self.parse(str, format)
      case format
      when /^application\/xml/
        @@xml_parser.parse str
      when /^application\/json/
        JSON.parse str
      else
        raise ArgumentError
      end
    end

    def self.build(data, format)
      return data if data.is_a?(String) || data.nil?
      case format
      when /xml/
        Gyoku.xml data, { key_converter: :none }
      when /json/
        data.to_json
      else
        raise ArgumentError
      end
    end

    %i(get post put delete).each do |method|
      define_method(method) do |*args|
        uri = args.shift
        body = (method == :post || method == :put) ? args.shift : nil
        params = args.shift
        options = args.shift || {}
        format = options[:format] || @format

        request = {
          method: method.downcase.to_sym,
          api_key: options[:api_key] || @api_key,
          uri: @base_uri + uri,
          params: params,
          format: format,
          retries: options[:retries] || @retries,
          debug: @debug
        }
        request[:body] = self.class.build(body, format) if body

        response = Request.new(request).response

        # API:et skickar tillbaka tom respons vid DELETE.
        if !response.body.to_s.empty? && (options.key?(:parse) ? options[:parse] : @parse)
          self.class.parse(response.body, format)
        else
          response.body
        end
      end
    end

    def sort_by_keys(hash, keys, reverse = [])
      if keys.is_a? String
        keys = keys.to_s.split(',').map { |k| k.to_sym }
      end

      if reverse.is_a? String
        reverse = reverse.to_s.split(',').map { |k| k.to_sym }
      end

      hash.sort do |a, b|
        order = 0
        keys.each do |k|
          order = (a[k] || '') <=> (b[k] || '')
          order = -order if reverse&.include? k
          break order unless order == 0
        end
        order
      end
    end

    # Metoder vars namn följer Ex Libris dokumentation
    #
    def add_new_course(body:, options: {})
      post("/almaws/v1/courses", body, nil, options)
    end

    def get_user_details(user_id:, params: nil, options: {})
      get("/almaws/v1/users/#{user_id}", params, options)
    end

    def update_user_details(user_id:, body:, params: nil, options: {})
      put("/almaws/v1/users/#{user_id}", body, params, options)
    end

    def remove_course(course_id:, options: {})
      delete("/almaws/v1/courses/#{course_id}", nil, options)
    end

    def get_user_fees(user_id:, params: nil, options: {})
      get("/almaws/v1/users/#{user_id}/fees", params, options)
    end

    def pay_user_fees(user_id:, body: nil, params: nil, options: {})
      post("/almaws/v1/users/#{user_id}/fees/all", body, params, options)
    end

    def retrieve_users_resource_sharing_request(user_id:, request_id:, params: nil, options: {})
      get("/almaws/v1/users/#{user_id}/resource_sharing_requests/#{request_id}", params, options)
    end

    def cancel_user_request(user_id:, request_id:, params: nil, options: {})
      delete("/almaws/v1/users/#{user_id}/requests/#{request_id}", params, options)
    end

    def create_partner(body:, options: {})
      post("/almaws/v1/partners", body, nil, options)
    end

    def create_user(body:, params: nil, options: {})
      post("/almaws/v1/users", body, params, options)
    end

    def create_reading_list(course_id:, body:, params: nil, options: {})
      post("/almaws/v1/courses/#{course_id}/reading-lists", body, params, options)
    end

    def retrieve_courses(params: nil, options: {})
      get("/almaws/v1/courses", params, options)
    end

    def retrieve_course(course_id:, params: nil, options: {})
      get("/almaws/v1/courses/#{course_id}", params, options)
    end

    def retrieve_partner(partner_code:, params: nil, options: {})
      get("/almaws/v1/partners/#{partner_code}", params, options)
    end

    def retrieve_items_list(mms_id:, holding_id:, params: nil, options: {})
      get("/almaws/v1/bibs/#{mms_id}/holdings/#{holding_id}/items", params, options)
    end

    def retrieve_holdings_list(mms_id:, options: {})
      get("/almaws/v1/bibs/#{mms_id}/holdings", nil, options)
    end

    def retrieve_holdings_record(mms_id:, holding_id:, options: {})
      get("/almaws/v1/bibs/#{mms_id}/holdings/#{holding_id}", nil, options)
    end

    def retrieve_user_requests_per_bib(mms_id:, params: nil, options: {})
      get("/almaws/v1/bibs/#{mms_id}/requests", params, options)
    end

    def retrieve_reading_lists(course_id:, reading_list_id: nil, params: nil, options: {})
      if reading_list_id.nil?
        get("/almaws/v1/courses/#{course_id}/reading-lists", params, options)
      else
        get("/almaws/v1/courses/#{course_id}/reading-lists/#{reading_list_id}", params, options)
      end
    end

    def loan_by_item_information(mms_id:, holding_id:, item_id:, params: nil, options: {})
      get("/almaws/v1/bibs/#{mms_id}/holdings/#{holding_id}/items/#{item_id}/loans", params, options)
    end

    def get_requested_resources(params: nil, options: {})
      get('/almaws/v1/task-lists/requested-resources', params, options)
    end

    def retrieve_analytics_report(params: nil, simplify: false, options: {})
      result = get('/almaws/v1/analytics/reports', params, options)
      query_result = result['report']['QueryResult']

      if simplify
        rows = [query_result['ResultXml']['rowset']['Row']].flatten
        result = rows.map do |r|
          # Kompensera för att Alma inte skickar tillbaka tomma kolumner.
          columns = []
          # Förutsätter att varje kolumnnamn slutar med siffra som
          # anger ordningen, t.ex. "Column0".
          unless r.nil?
            r.each { |key, value| columns[key[-1].to_i] = value }
          end
          columns
        end
      end

      if query_result['IsFinished']
        return result
      else
        params['token'] = query_result['ResumptionToken'] || params['token']
        add_result = retrieve_analytics_report(params: params, simplify: simplify, options: options)
        return simplify ? result + add_result : [result, add_result].flatten
      end
    end

    def update_course(course_id:, body:, params: nil, options: {})
      put("/almaws/v1/courses/#{course_id}", body, params, options)
    end

    def update_reading_list(course_id:, reading_list_id:, body:, params: nil, options: {})
      put("/almaws/v1/courses/#{course_id}/reading-lists/#{reading_list_id}", body, params, options)
    end

    # Metoder med avvikande namn och parameterstruktur (x-prefixet
    # betyder eXtension)
    #
    def x_get_item_by_barcode(barcode, options)
      get('/almaws/v1/items', { item_barcode: barcode }, options)
    end

    def x_get_items(
          mms_ids:,
          sort_by: nil,
          reverse: nil,
          group_by: nil,
          extra_item_fields: []
        )

      items = []

      [*mms_ids].each do |mms_id|
        items_list = retrieve_items_list(
          mms_id: mms_id,
          holding_id: 'ALL',
          params: { limit: 100, expand: 'due_date' }
        )
        next if items_list['item'].nil?

        items_list['item'].each do |i|
          item = {
            mms_id: mms_id,
            holding_id: i['holding_data']['holding_id'],
            call_number: i['holding_data']['call_number'],
            library: i['item_data']['library']['value'],
            location: i['item_data']['location']['value'],
            location_desc: i['item_data']['location']['desc'],
            policy: i['item_data']['policy']['value'],
            base_status: i['item_data']['base_status']['value'],
            process_type: i['item_data']['process_type']['value'],
            description: i['item_data']['description'],
            barcode: i['item_data']['barcode'],
            due_date: i['item_data']['due_date'],
          }

          if i['holding_data']['in_temp_location']
            item[:temp_location] = {
              library: i['holding_data']['temp_library']['value'],
              location: i['holding_data']['temp_location']['value'],
              location_desc: i['holding_data']['temp_location']['desc'],
            }
            item[:permanent_location] = {
              library: item[:library],
              location: item[:location],
              location_desc: item[:location_desc],
            }
          end

          extra_item_fields.each { |f| item[f.to_sym] = i['item_data'][f.to_s] }
          items.push item
        end
      end

      if sort_by
        items = sort_by_keys(items, sort_by, reverse)
      end

      if group_by
        group_fields = group_by.to_s.split(',').map do |f|
          Proc.new { |a| a[f.to_sym] }
        end
        items = items.nested_group_by(*group_fields)
      end

      items
    end

    def x_get_holdings(
          mms_ids:,
          group_by: nil,
          extra_item_fields: [],
          sloppy_mode: false
        )

      items = x_get_items(
        mms_ids: mms_ids,
        extra_item_fields: extra_item_fields
      )

      holdings = {}

      # Extrahera (unika) holdings från exemplar.
      if items.size > 0
        items.each do |i|
          # Tillfälliga placeringar saknar egen holding (det ser ut
          # såhär i API:et). Lägg därför till en "holding" med relevanta
          # uppgifter i förekommande fall.
          if i[:temp_location]
            holding_key = i[:temp_location].to_s.to_sym

            # TODO: hantera temporary call number
            holdings[holding_key] ||= {
              mms_id: i[:mms_id],
              holding_id: nil,
              is_temp_holding: true,
              call_number: i[:call_number],
              library: i[:temp_location][:library],
              location: i[:temp_location][:location],
              location_desc: i[:temp_location][:location_desc],
              items: []
            }
            i.delete(:temp_location)
          else
            holding_key = i[:holding_id].to_sym

            # Ska vara samma för unikt holding ID.
            holdings[holding_key] ||= {
              mms_id: i[:mms_id],
              holding_id: i[:holding_id],
              is_temp_holding: false,
              call_number: i[:call_number],
              library: i[:library],
              location: i[:location],
              location_desc: i[:location_desc],
              items: []
            }
          end

          i.delete(:mms_id)
          i.delete(:call_number)
          i.delete(:library)
          i.delete(:location)
          i.delete(:location_desc)

          holdings[holding_key][:items].push(i)
        end
      end

      # För holdings som saknas exemplar krävs ett extra
      # API-uppslag. Vid "sloppy mode" hoppas detta över (såvida inte
      # posten helt saknar exemplar). För de flesta poster gäller
      # nämligen (i vårt fall) att om en holding har exemplar har
      # samtliga det.
      if items.size < 1 || !sloppy_mode
        [*mms_ids].each do |mms_id|
          holdings_list = retrieve_holdings_list(mms_id: mms_id)
          next if holdings_list['holding'].nil?

          holdings_list['holding'].each do |h|
            holdings[h['holding_id'].to_sym] ||= {
              mms_id: mms_id,
              holding_id: h['holding_id'],
              is_temp_holding: false,
              call_number: h['call_number'],
              library: h['library']['value'],
              location: h['location']['value'],
              location_desc: h['location']['desc'],
              items: []
            }
          end
        end
      end

      holdings = holdings.map { |k, v| v }

      if group_by
        holdings = holdings.group_by { |h| h[group_by.to_sym] }
      end

      holdings
    end

    def x_get_reading_lists(course_code:)
      # TODO: OBS! Felhantering
      course = retrieve_courses(params: {q: "code~#{course_code}"})
      reading_lists = retrieve_reading_lists(course_id: course['course'][0]['id'])

      reading_lists['reading_list'].each do |list|
        full_list = retrieve_reading_lists(
          course_id: course['course'][0]['id'],
          reading_list_id: list['id'],
          params: { view: 'full' }
        )
        list['citations'] = full_list['citations']['citation']
      end

      reading_lists
    end
  end
end
