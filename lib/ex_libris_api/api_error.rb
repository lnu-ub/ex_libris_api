module ExLibrisAPI
  class APIError < StandardError
    attr_reader :codes
    attr_reader :rest_e

    def initialize(msg = nil, rest_e)
      @rest_e = rest_e
      @codes = []
      begin
        content_type = rest_e.response.net_http_res.header['content-type']
        response = API.parse(rest_e.response.body, content_type)
        # Harmonisera XML/JSON
        response = response['web_service_result'] if response.has_key? 'web_service_result'
        if response['errorsExist']
          errors = [response['errorList']['error']].flatten
          @codes = errors.map do |e|
            e['errorCode']
          end
        end
      rescue
        #
      end
      super(msg)
    end
  end
end
