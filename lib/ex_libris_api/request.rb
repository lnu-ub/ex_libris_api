require 'rest-client'
require 'addressable'

module ExLibrisAPI
  class Request
    attr_reader :response

    def initialize(
          method:,
          api_key:,
          uri:,
          body: '',
          params: nil,
          format: 'application/json;charset=UTF-8',
          retries: 5,
          debug: false
        )

      raise ArgumentError unless %i(get post put delete).include? method

      RestClient.log = 'stdout' if debug

      @response = nil

      begin
        if method == :get || method == :delete
          @response = RestClient.send(
            method,
            format_uri(uri, params),
            {
              'Authorization' => "apikey #{api_key}",
              'Accept' => format
            }
          )
        else
          @response = RestClient.send(
            method,
            format_uri(uri),
            body,
            {
              params: params,
              'Authorization' => "apikey #{api_key}",
              'Content-type' => format,
              'Accept' => format
            }
          )
        end
      rescue RestClient::Exception => e
        if e.http_code == 429 && retries > 0
          retries -= 1
          sleep(rand + 0.2)
          retry
        end

        api_error = APIError.new(method.to_s + ' failed', e)
        if debug
          STDERR.puts api_error.codes.inspect
          STDERR.puts api_error.rest_e.response.body
        end
        raise api_error
      end
    end

    def format_uri(uri, params = nil)
      uri = Addressable::URI.parse(uri)
      uri.query_values = params if params
      uri.normalize.to_s
    end
  end
end
